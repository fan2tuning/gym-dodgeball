# gym-dodgeball

Custom OpenAI gym for an dodgeball environment.

### Installation
Install the python package directly from the repository with:
```bash
> pip install -e git+https://gitlab.com/fan2tuning/gym-dodgeball.git#egg=gym_dodgeball
```

### Minimum code usage

Import it and use in in a python script as following:
```python
import gym
import gym_dodgeball

env = gym.make("dodgeball-v0")
obs = env.reset()
for i in range(10):
    env.render()
    obs, rew, done,_ = env.step(env.action_space.sample())
    if done:
        break
env.close()
```

### Development

Please ensure that there are no linting errors when committing code to the repository:
```bash
> flake8
> pylint --disable=missing-module-docstring,too-many-instance-attributes gym_dodgeball
```

Please ensure that the unit tests doesn't output errors when committing code to the repository:
```bash
> pip install -U pytest
> cd ~/gym-dodgeball  # "~/gym-dodgeball" being the folder with your setup.py file
> export PYTHONPATH="./"
> pytest gym_dodgeball/tests
```

Using Yapf for pep8 compliance:
```bash
> pip install yapf
> cd ~/gym-dodgeball  # "~/gym-dodgeball" being the folder with your setup.py file
> yapf -r . -i # Will change files inplace for the whole folder recursively.
```

### Rendering and recording.

For rendering, the module currently support animated matplotlib graph, that can pass data in rgba format through env.render()

If you need to execute this code inside a Docker Container, you can use an X server on your host machine and change the DISPLAY variable:
```bash
> export DISPLAY=host.docker.internal:0
```

For reference : https://github.com/microsoft/vscode-remote-release/issues/616 and https://github.com/microsoft/vscode-remote-release/issues/550 

If you want to record to .mp4, you can use the set_renderer method to use recording. You will need to install ffmpeg first.
This module use python-ffmpeg internaly as a binder to call ffmpeg 

```bash
> sudo apt install ffmpeg
```

```python
> env.set_renderer(generate_movie=True)
>
> for i in range(50):
>    action = env.action_space.sample()
>    obs, reward, done, info = env.step(action)
>    env.render()
env.close()
```
