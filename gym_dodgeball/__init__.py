from gym.envs.registration import register

register(
    id='dodgeball-v0',
    entry_point='gym_dodgeball.envs:DodgeballEnv',
)
