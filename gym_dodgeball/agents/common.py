# pylint: disable=invalid-name, too-many-arguments

import numpy as np


class GymItem():
    """Default gym object class, handle its physics."""
    def __init__(self, map_size, x=None, y=None, vx=0, vy=0):
        self.env_map_size = map_size
        self.x = x
        self.y = y
        self.vx = vx
        self.vy = vy
        # Used for visualisation
        self.color = None
        self.marker = None
        self.size = None
        self.zorder = None

    def get_speed(self):
        """Compute absolute speed of object."""
        return np.sqrt(self.vx**2 + self.vy**2)

    def get_distance(self, x_object, y_object):
        """Compute the distance of the item to another object."""
        return np.sqrt((self.x - x_object)**2 + (self.y - y_object)**2)

    def get_features(self, features):
        """ Return np.array of features of the object."""
        # WIP : change for a better approach
        if features == ['type', 'x', 'y', 'vx', 'vy']:
            return np.array([1, self.x, self.y, self.vx, self.vy])
        raise NotImplementedError(
            f"Features requested are not yet implemented: {features}")
