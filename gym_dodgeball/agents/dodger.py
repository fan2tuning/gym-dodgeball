# pylint: disable=invalid-name

from enum import Enum

import numpy as np

from gym_dodgeball.agents.common import GymItem


class DodgerAgentActions(Enum):
    """
    Enumerate the different actions the shooter agent can perform:
    """
    UP = "UP"
    DOWN = "DOWN"
    LEFT = "LEFT"
    RIGHT = "RIGHT"


class DodgerAgent(GymItem):
    """
    Dodger agent class, handle its physics and actions.

    :param map_size: size of environment map. ex: (20,20)

    :var RADIUS: distance to be hit by a projectile
    :var MAX_SPEED: speed limit of the dodger agent
    :var ACCELERATION: acceleration norm of the dodger agent when moving
    """

    RADIUS = 1  # m
    MAX_SPEED = 3  # m.s-1
    ACCELERATION = 2  # m.s-2

    def __init__(self, *args, **kwargs):
        super(DodgerAgent, self).__init__(*args, **kwargs)
        self.is_hit = False
        self.color = 'green'
        self.marker = 'o'
        self.size = 500.0

    def act(self, dt, orders=None):
        """
        Simulate physics and self.get_speed()actions of DodgerAgent.
        :param dt: timestep of the simulation
        :param orders: list of actions asked to the Agent
        """
        ax = 0
        ay = 0
        if orders is not None:
            actions = [DodgerAgentActions[order] for order in orders]
            for action in actions:
                if action == DodgerAgentActions.UP:
                    ay += self.ACCELERATION
                elif action == DodgerAgentActions.DOWN:
                    ay -= self.ACCELERATION
                elif action == DodgerAgentActions.RIGHT:
                    ax += self.ACCELERATION
                elif action == DodgerAgentActions.LEFT:
                    ax -= self.ACCELERATION
                else:
                    raise ValueError(f"Unknown error with action: {action}.")

        # limit acceleration, basically just diagonal moves
        absolute_acceleration = np.sqrt(ax**2 + ay**2)
        if absolute_acceleration > self.ACCELERATION:
            ax = ax * self.ACCELERATION / absolute_acceleration
            ay = ay * self.ACCELERATION / absolute_acceleration

        # update coordinates
        self.x += 0.5 * ax * dt**2 + self.vx * dt
        self.y += 0.5 * ay * dt**2 + self.vy * dt

        # update and limit speed
        self.vx += ax * dt
        self.vy += ay * dt
        absolute_speed = self.get_speed()
        if absolute_speed > self.MAX_SPEED:
            self.vx = self.vx * self.MAX_SPEED / absolute_speed
            self.vy = self.vy * self.MAX_SPEED / absolute_speed

        # include map boundaries, limit position and reinitialise speed
        if abs(self.x) > self.env_map_size[0] * 0.5:
            self.x -= np.sign(
                self.x) * (abs(self.x) - self.env_map_size[0] * 0.5)
            self.vx = 0
        if abs(self.y) > self.env_map_size[1] * 0.5:
            self.y -= np.sign(
                self.y) * (abs(self.y) - self.env_map_size[1] * 0.5)
            self.vy = 0
