# pylint: disable=invalid-name

from gym_dodgeball.agents.common import GymItem


class Projectile(GymItem):
    """
    Projectile class fired by the Shooter Agent.
    """
    def __init__(self, *args, **kwargs):
        super(Projectile, self).__init__(*args, **kwargs)
        self.dead_state = False
        self.speed = self.get_speed()
        self.color = 'red'
        self.marker = '+'
        self.size = 30.0

    def act(self, dt, ax=0, ay=0):
        """
        Simulate physics of Projectile.

        :param dt: timestep of the simulation
        :param ax: acceleration parameter on x axis
        :param ay: acceleration parameter on y axis
        """
        # Physics
        self.x += 0.5 * ax * dt**2 + self.vx * dt
        self.y += 0.5 * ay * dt**2 + self.vy * dt
        self.vx += ax * dt
        self.vy += ay * dt
        self.speed = self.get_speed()

        # Out of the map
        out_of_bounds_x = abs(self.x) > self.env_map_size[0] * 0.5
        out_of_bounds_y = abs(self.y) > self.env_map_size[1] * 0.5
        if out_of_bounds_x or out_of_bounds_y:
            self.dead_state = True

        return self.dead_state
