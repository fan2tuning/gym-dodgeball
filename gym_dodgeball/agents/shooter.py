# pylint: disable=invalid-name

from enum import Enum

import math
import random

import numpy as np

from gym_dodgeball.agents.common import GymItem
from gym_dodgeball.agents.projectiles import Projectile


class ShooterMode(Enum):
    """
    Enumerate the different shooter agent modes:
        - default : shoot to target with a deviation
        - motion : default parameter but agent moves
        - random : shoot randomly
        - pro : shoot fast and precisely
    """
    DEFAULT = "default"
    MOTION = "motion"
    RANDOM = "random"
    PRO = "pro"

    # WIP : add gatling mode without crashing the observation
    # GATLING = "gatling"

    def __init__(self, mode):
        self.mode = mode
        self.reload_time = None
        self.agent_speed = None
        self.projectile_speed = None
        self.deviation_max = None

        if self.mode == "default":
            self._default_init()
        elif self.mode == "motion":
            self._motion_init()
        elif self.mode == "pro":
            self._pro_init()
        elif self.mode == "random":
            self._random_init()
        else:
            raise NotImplementedError(f"Shooter mode {self.mode} "
                                      "not implemented yet.")

    def _default_init(self):
        self.reload_time = 5.0  # s
        self.projectile_speed = 5.0  # m.s-1
        self.deviation_max = 15  # degrees
        self.agent_speed = 0  # m.s-1

    def _motion_init(self):
        # same as default mode but with a agent movement
        self._default_init()
        self.agent_speed = 1  # m.s-1

    def _pro_init(self):
        self._motion_init()
        self.reload_time = self.reload_time * 0.5  # s
        self.projectile_speed = self.projectile_speed * 1.5  # m.s-1
        self.deviation_max = 0  # degrees
        self.agent_speed = self.agent_speed * 3  # m.s-1

    def _random_init(self):
        # weird stats
        self._motion_init()

        reload_factor = random.randrange(50, 150) / 100.
        aspeed_factor = random.randrange(20, 300) / 100.
        pspeed_factor = random.randrange(20, 150) / 100.
        deviat_factor = 3.

        self.reload_time = self.reload_time * reload_factor  # s
        self.agent_speed = self.agent_speed * aspeed_factor  # m.s-1
        self.projectile_speed = self.projectile_speed * pspeed_factor  # m.s-1
        self.deviation_max = self.deviation_max * deviat_factor  # degrees


class ShooterAgent(GymItem):
    """
    Shooter agent class, handle its physics and its actions.

    :param map_size: size of environment map. ex: (20,20)
    :param mode: c.f ShooterMode
    """
    def __init__(self, mode="default", **kwargs):
        super(ShooterAgent, self).__init__(**kwargs)
        # init vars
        self.loc = None
        # Shooting param
        self.mode = ShooterMode(mode)
        timer_factor = random.randint(0, 100) / 100.
        self.reload_timer = self.mode.reload_time * timer_factor
        # init position & speed
        self._init_motion()

        self.color = 'red'
        self.marker = 'o'
        self.size = 500.0

    def set_projectile_speed_limit(self, limit):
        """Force limit the max speed of projectiles."""
        self.mode.projectile_speed = min(self.mode.projectile_speed, limit)

    def _init_motion(self):
        """
        Position randomly on the borders of the map. The map is supposed to
        be a square.
        """
        self.loc = random.choice(["horizontal", "vertical"])
        if self.loc == "horizontal":
            self.x = random.randrange(-self.env_map_size[0] * 0.5,
                                      self.env_map_size[0] * 0.5)
            self.y = random.choice(
                [-self.env_map_size[1] * 0.5, self.env_map_size[1] * 0.5])
            self.vx = self.mode.agent_speed * random.choice([-1, 1])
            self.vy = 0
        elif self.loc == "vertical":
            self.x = random.choice(
                [-self.env_map_size[0] * 0.5, self.env_map_size[0] * 0.5])
            self.y = random.randrange(-self.env_map_size[1] * 0.5,
                                      self.env_map_size[1] * 0.5)
            self.vx = 0
            self.vy = self.mode.agent_speed * random.choice([-1, 1])
        else:
            raise ValueError(self.loc)

    def act(self, dt, target=None):
        """
        Simulate physics and actions of ShooterAgent.

        :param dt: timestep of the simulation
        :param target: GymItem object in the environment targeted
        """
        # update agent position
        self.x += self.vx * dt
        self.y += self.vy * dt
        # restrict position to map size and update speed
        if abs(self.x) > self.env_map_size[0] * 0.5:
            self.x -= np.sign(
                self.x) * (abs(self.x) - self.env_map_size[0] * 0.5)
            self.vx = -self.vx
        if abs(self.y) > self.env_map_size[1] * 0.5:
            self.y -= np.sign(
                self.y) * (abs(self.y) - self.env_map_size[1] * 0.5)
            self.vy = -self.vy
        # agent reload and shoot
        self.reload_timer -= dt
        if (self.reload_timer <= 0) and (target is not None):
            new_projectile = self._shoot(target)
            self.reload_timer = self.mode.reload_time
            return new_projectile
        return None

    def _shoot(self, target):
        """Agent fire a projectile to a target, based on its shooting mode."""
        # vector to exact position
        dx = target.x - self.x
        dy = target.y - self.y
        angle = math.atan2(dy, dx)
        # adding deviation
        deviation = random.randint(-self.mode.deviation_max,
                                   self.mode.deviation_max)
        angle += math.radians(deviation)
        # projectile vector
        vx = math.cos(angle) * self.mode.projectile_speed
        vy = math.sin(angle) * self.mode.projectile_speed
        return Projectile(map_size=self.env_map_size,
                          x=self.x,
                          y=self.y,
                          vx=vx,
                          vy=vy)
