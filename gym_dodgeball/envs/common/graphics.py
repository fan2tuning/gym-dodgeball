# pylint: disable=invalid-name

from enum import Enum
import ffmpeg
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors


# Import from matplotlib codebase
class BlitManager:
    """
        Blit manager facilitate rendering with blitting in matplotlib.
    """
    def __init__(self, canvas, animated_artists=()):
        """
        Parameters
        ----------
        canvas : FigureCanvasAgg
            The canvas to work with, this only works for sub-classes of the Agg
            canvas which have the `~FigureCanvasAgg.copy_from_bbox` and
            `~FigureCanvasAgg.restore_region` methods.

        animated_artists : Iterable[Artist]
            List of the artists to manage
        """
        self.canvas = canvas
        self._bg = None
        self._artists = []

        for a in animated_artists:
            self.add_artist(a)
        # grab the background on every draw
        self.cid = canvas.mpl_connect("draw_event", self.on_draw)

    def on_draw(self, event):
        """Callback to register with 'draw_event'."""
        cv = self.canvas
        if event is not None:
            if event.canvas != cv:
                raise RuntimeError
        self._bg = cv.copy_from_bbox(cv.figure.bbox)
        self._draw_animated()

    def add_artist(self, art):
        """
        Add an artist to be managed.

        Parameters
        ----------
        art : Artist

            The artist to be added.  Will be set to 'animated' (just
            to be safe).  *art* must be in the figure associated with
            the canvas this class is managing.

        """
        if art.figure != self.canvas.figure:
            raise RuntimeError
        art.set_animated(True)
        self._artists.append(art)

    def remove_artist(self, art):
        """
        Add an artist to be managed.

        Parameters
        ----------
        art : Artist

            The artist to be removed.
        """
        self._artists.remove(art)

    def _draw_animated(self):
        """Draw all of the animated artists."""
        fig = self.canvas.figure
        for a in self._artists:
            fig.draw_artist(a)

    def update(self):
        """Update the screen with animated artists."""
        cv = self.canvas
        fig = cv.figure
        # paranoia in case we missed the draw event,
        if self._bg is None:
            self.on_draw(None)
        else:
            # restore the background
            cv.restore_region(self._bg)
            # draw all of the animated artists
            self._draw_animated()
            # update the GUI state
            cv.blit(fig.bbox)
        # let the GUI event loop process anything it has to do
        cv.flush_events()


class RenderingModes(Enum):
    """List the different rendering modes implemented."""
    matplotlib = "matplotlib"
    rgb = "rgb"

    def __init__(self, mode):
        self.mode = mode


class EnvViewer():
    """Viewer to render a dodgeball environment."""

    FPS = 60

    def __init__(self, env, mode, path, generate_movie=False):
        self.env = env
        self.mode = RenderingModes(mode).mode
        self.generate_movie = generate_movie
        self.path = path
        self.ffmpeg_pipeline = None

        if self.mode == "matplotlib":
            self.fig, self.ax = plt.subplots(figsize=(6, 6))
            plt.xlim(-self.env.MAP_SIZE[0] * 0.5, self.env.MAP_SIZE[0] * 0.5)
            plt.ylim(-self.env.MAP_SIZE[1] * 0.5, self.env.MAP_SIZE[1] * 0.5)
            plt.xticks(
                np.arange(-self.env.MAP_SIZE[0] * 0.5,
                          self.env.MAP_SIZE[0] * 0.5, 1))
            plt.yticks(
                np.arange(-self.env.MAP_SIZE[1] * 0.5,
                          self.env.MAP_SIZE[1] * 0.5, 1))
            plt.title("gym-dodgeball")
            plt.grid(b=True,
                     color='lightgrey',
                     linestyle='-',
                     linewidth=1,
                     zorder=1)

            self.sca_artist_agents = self.ax.scatter([], [],
                                                     cmap="jet",
                                                     edgecolor="k")
            self.bm = BlitManager(self.fig.canvas, [self.sca_artist_agents])
            plt.show(block=False)
            plt.pause(.1)

    def display(self):
        """Display the data according to the rendering mode selected."""
        # matplotlib grapghs saved as png instances
        if self.mode == "matplotlib":
            # Order is important
            agents = [
                self.env.dodger_agent, *self.env.shooter_agents,
                *self.env.projectiles
            ]
            self.sca_artist_agents.set_offsets([[agent.x, agent.y]
                                                for agent in agents])
            self.sca_artist_agents.set_sizes([agent.size for agent in agents])
            self.sca_artist_agents.set_facecolors(
                np.array([colors.to_rgb(agent.color) for agent in agents]))

            # Quiver and arrows need to be redrawn each time, because they
            # can't have variable number of arrows.
            qui_artist_agents = self.ax.quiver([agent.x for agent in agents],
                                               [agent.y for agent in agents],
                                               [agent.vx for agent in agents],
                                               [agent.vy for agent in agents],
                                               angles='xy',
                                               scale_units='xy',
                                               scale=1)

            self.bm.add_artist(qui_artist_agents)
            self.bm.update()
            self.bm.remove_artist(qui_artist_agents)

            data = np.frombuffer(self.fig.canvas.tostring_rgb(),
                                 dtype=np.uint8)
            data = data.reshape(self.fig.canvas.get_width_height()[::-1] +
                                (3, ))

            if self.generate_movie:
                if self.ffmpeg_pipeline is None:
                    # Define pipeline
                    self.ffmpeg_pipeline = (ffmpeg.input(
                        'pipe:',
                        format='rawvideo',
                        pix_fmt='rgb24',
                        s='{}x{}'.format(data.shape[1], data.shape[0])).output(
                            self.path + "/movie.mp4",
                            pix_fmt='yuv420p',
                            vcodec="libx264",
                            r=EnvViewer.FPS).overwrite_output().run_async(
                                pipe_stdin=True))
                # Process the data to be recorded
                self.ffmpeg_pipeline.stdin.write(
                    data.astype(np.uint8).tobytes())
            # Return data if needed for further processing.
            return data
        raise NotImplementedError(f"Rendering mode {self.mode} not "
                                  "implemented.")

    def close(self):
        """Close the graphics environment."""
        if self.mode == "matplotlib":
            plt.close()
            if self.generate_movie:
                self.ffmpeg_pipeline.stdin.close()
                self.ffmpeg_pipeline.wait()
        else:
            raise NotImplementedError(f"Rendering mode {self.mode} not "
                                      "implemented.")
