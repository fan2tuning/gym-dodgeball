# pylint: disable=undefined-loop-variable

import numpy as np

from gym import spaces


class ObservationType():
    """
    Astract class of observations. Function space shall return the observation
    space and observe return the observation of the current environment.
    """
    def space(self):
        """Return space of the environment."""
        raise NotImplementedError()

    def observe(self):
        """Return observation of the environment."""
        raise NotImplementedError()


class ObservationError(Exception):
    """Specific error for Observations."""


class OmniscientObservation(ObservationType):
    """
    Observation of the kinematic of projectiles and agents.
    In this observation the agent is absolutely omniscient.
    Absolute coordinates are used.
    """

    FEATURES = ['type', 'x', 'y', 'vx', 'vy']

    def __init__(self, env, observed_items=10):
        """
        :param env:environment to observe
        :param observed_items: maximum number of items observed in the env
        """
        self.env = env
        self.observed_items = observed_items
        self.obs_shape = (self.observed_items, len(self.FEATURES))
        self.obs_dtype = np.float32

    def space(self):
        return spaces.Box(shape=self.obs_shape,
                          low=-1,
                          high=1,
                          dtype=self.obs_dtype)

    def _normalise_obs(self, obs):
        """
        Normalise between -1:1 the observation of following features for the
        dodger agent, the shooter agents and the projectiles.
            FEATURES = ['type', 'x', 'y', 'vx', 'vy']
        """
        # 'type' is already normalised
        # normalise position 'x'
        obs[:, 1] = obs[:, 1] / (0.5 * self.env.MAP_SIZE[0])
        # normalise position 'y'
        obs[:, 2] = obs[:, 2] / (0.5 * self.env.MAP_SIZE[1])
        # normalise speed 'vx' & 'vy'
        obs[:, 3:5] = obs[:, 3:5] / self.env.MAX_SPEED
        return obs

    def observe(self):
        """
        Observe the environment and return the np.array of FEATURES
        for self, shooter agents and each projectile.
            FEATURES = ['type', 'x', 'y', 'vx', 'vy']
        """
        obs = np.zeros(self.obs_shape, dtype=self.obs_dtype)

        # observe self
        obs[0, :] = self.env.dodger_agent.get_features(self.FEATURES)

        # observe shooter agents
        for i, shooter_agent in enumerate(self.env.shooter_agents):
            if (i + 1) >= self.obs_shape[0]:
                raise ObservationError(
                    "Too many shooter agents to observe. Observation can track"
                    f"{self.obs_shape[0]} objects in total but there is "
                    f"{i+1} agents")
            obs[i + 1, :] = shooter_agent.get_features(self.FEATURES)

        # observe projectiles
        for k, projectile in enumerate(self.env.projectiles):
            if (i + k + 2) >= self.obs_shape[0]:
                raise ObservationError(
                    "Too many projectiles to observe. Observation can track"
                    f"{self.obs_shape[0]} objects in total but there is "
                    f"{k+1} projectiles")
            obs[i + k + 2, :] = projectile.get_features(self.FEATURES)

        return self._normalise_obs(obs)
