# pylint: disable=unused-argument

from enum import Enum

import numpy as np


class RewardMode(Enum):
    """
    Enumerate the different reward modes:
        - simple : +1 each step alive
        - speed : greater reward for high speed
        - slow : greater reward for low speed or immobile
        - close : greater reward for being close to projectiles
        - distant : greater reward for being far away of projectiles
    """
    simple = "simple"
    speed = "speed"
    slow = "slow"
    close = "close"
    distant = "distant"

    def __init__(self, mode):
        self.mode = mode

    def get(self, env, action=None):
        """Return the reward value for the selected mode."""
        max_dist = 0.5 * np.sqrt(env.MAP_SIZE[0]**2 + env.MAP_SIZE[1]**2)

        # when agent is hit
        if env.dodger_agent.is_hit:
            return -1

        # simple reward of surviving
        if self.mode == "simple":
            return 1

        # reward [0,1], linear scaling to max speed
        if self.mode == "speed":
            return env.dodger_agent.get_speed() / env.MAX_SPEED

        # reward [0,1], invert linear scaling to max speed
        if self.mode == "slow":
            return 1 - (env.dodger_agent.get_speed() / env.MAX_SPEED)

        # reward [0,1], invert linear scaling of distance to projectiles
        if self.mode == "close":
            min_dist = max_dist.copy()
            for proj in env.projectiles:
                min_dist = min(min_dist,
                               env.dodger_agent.get_distance(proj.x, proj.y))
            return 1 - (min_dist / max_dist)

        # reward [0,1], linear scaling of distance to projectiles
        if self.mode == "distant":
            min_dist = 0
            for proj in env.projectiles:
                min_dist = min(min_dist,
                               env.dodger_agent.get_distance(proj.x, proj.y))
            return min_dist / max_dist

        raise NotImplementedError(f"Reward mode {self.mode} "
                                  "not implemented yet.")
