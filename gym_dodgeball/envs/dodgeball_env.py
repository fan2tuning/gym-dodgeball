# pylint: disable=too-many-instance-attributes, too-many-arguments
import os
import time
import numpy as np
import gym
from gym import spaces
from gym.utils import seeding

from gym_dodgeball.envs.common.rewards import RewardMode
from gym_dodgeball.envs.common.observations import ObservationType
from gym_dodgeball.envs.common.observations import OmniscientObservation
from gym_dodgeball.envs.common.graphics import EnvViewer
from gym_dodgeball.agents.dodger import DodgerAgent
from gym_dodgeball.agents.shooter import ShooterAgent
from gym_dodgeball.agents.shooter import ShooterMode


class DodgeballEnv(gym.Env):
    """
    Dodgeball environment. The shooters in this env are default bots piloted
    by the env. OmniscientObservation is used, meaning the dodging agent is
    omniscient. Action space is MultiBinary, meaning the Agent can perform
    several actions at the same time (=pushing several buttons).

    :param n_shooter: number of shooter bots
    :param shooter_mode: mode of shooter bots ('default', 'motion' etc)
    :param reward_mode: reward policy applied ('simple', 'speed' etc)
    :param action_space_type: action space class used for the simulation
    :param observation_type: observation class used for the simulation

    :var INITIAL_POSITION: initial coordinates of the dodger agent
    :var ACTIONS: actions of the dodger agent authorized by the env
    :var TIMESTEP: time in seconds between each step simulation
    :var MAX_SPEED: maximum speed of objects authorized by the environment
    :var MAP_SIZE: size of the current map, defined as a square centered in 0,0
    """

    INITIAL_POSITION = [0, 0]
    DEFAULT_RENDERER = "matplotlib"
    DEFAULT_IMG_PATH = "./video/"
    ACTIONS = {0: 'UP', 1: 'DOWN', 2: 'RIGHT', 3: 'LEFT'}
    TIMESTEP = 0.1  # s
    MAP_SIZE = (20, 20)  # m
    MAX_SPEED = 10  # m.s-1

    def __init__(self,
                 n_shooter=4,
                 shooter_mode="default",
                 reward_mode="simple",
                 action_space_type: spaces.Space = spaces.MultiBinary,
                 observation_type: ObservationType = OmniscientObservation):
        print("Initialiase DodgeballEnv object")

        # Configuration
        self.reward_mode = RewardMode(reward_mode)
        self.n_shooter = n_shooter
        self.shooter_mode = ShooterMode(shooter_mode).mode

        # Agents
        self.dodger_agent = None
        self.shooter_agents = None
        self.projectiles = None

        # Spaces
        self.observation = None
        self.action_space = None
        self.action_space_type = action_space_type
        self.observation_space = None
        self.observation_type = observation_type

        # Running
        self.done = False
        self.steps = None

        # Rendering
        self.render_mode = None
        self.render_path = None
        self.render_is_movie = False
        self.viewer = None

        self.seed()

    def seed(self, seed=None):
        self.np_random, seed = seeding.np_random(seed)
        return [seed]

    def _define_spaces(self):
        self.observation = self.observation_type(self)
        self.observation_space = self.observation.space()
        self.action_space = self.action_space_type(len(self.ACTIONS))

    def reset(self):
        # initialise viewer
        if self.viewer is None:
            self.viewer = EnvViewer(env=self,
                                    mode=self.render_mode
                                    or self.DEFAULT_RENDERER,
                                    path=self.render_path
                                    or self.DEFAULT_IMG_PATH,
                                    generate_movie=self.render_is_movie)
        self.steps = 0
        # initialise dodger agent
        self.dodger_agent = DodgerAgent(map_size=self.MAP_SIZE,
                                        x=self.INITIAL_POSITION[0],
                                        y=self.INITIAL_POSITION[1])
        # initialise shooter agents
        self.shooter_agents = []
        for _ in range(self.n_shooter):
            new_agent = ShooterAgent(self.shooter_mode, map_size=self.MAP_SIZE)
            new_agent.set_projectile_speed_limit(self.MAX_SPEED)
            self.shooter_agents.append(new_agent)
        # initialise projectiles
        self.projectiles = []

        self._define_spaces()
        return self.observation.observe()

    def step(self, action):
        if self.shooter_agents is None:
            raise NotImplementedError("The shooter agents must be initialised "
                                      "before running a environment step.")
        if self.dodger_agent is None:
            raise NotImplementedError("The dodger agent must be initialised "
                                      "before running a environment step.")
        if self.projectiles is None:
            raise NotImplementedError("The projectiles must be initialised "
                                      "before running a environment step.")
        self.steps += 1
        # WIP ; add steps skipping possibilities
        # for example several simulate functions with constant actions
        self._simulate(action)

        observation = self.observation.observe()
        done = self._is_done()
        reward = self.reward_mode.get(self, action)
        info = {}

        return observation, reward, done, info

    def _make_dodger_act(self, actions):
        # Interpret actions from the Agent to environment orders
        # [1,0,0,1] -> ["UP","LEFT"]
        if actions is not None:
            if len(self.ACTIONS) != len(actions):
                raise ValueError(f"Action input: {actions} does not match the "
                                 "allowed actions in this environment: "
                                 f"{self.ACTIONS}.")
            list_orders = [self.ACTIONS[i] for i in np.where(actions)[0]]
            self.dodger_agent.act(self.TIMESTEP, list_orders)
        else:
            self.dodger_agent.act(self.TIMESTEP)

    def _simulate(self, actions=None):
        """
        Perform step(s) of the simulation based on the action provided.
        """

        # WIP : loop for with constant actions to skip steps
        # WIP: Reimplement to dict switch case ?
        if self.action_space_type == spaces.Discrete:
            to_multibinary_actions = np.zeros((len(self.ACTIONS), ))
            to_multibinary_actions[actions] = 1
            self._make_dodger_act(to_multibinary_actions)
        elif self.action_space_type == spaces.MultiBinary:
            self._make_dodger_act(actions)
        else:
            raise NotImplementedError("Action space is not implemented for "
                                      "simulation.")

        # Actions of all shooter agents
        for shooter_agent in self.shooter_agents:
            new_projectile = shooter_agent.act(dt=self.TIMESTEP,
                                               target=self.dodger_agent)
            if new_projectile:
                self.projectiles.append(new_projectile)

        # Simulation of projectiles, deletion of dead ones
        dead_status = []
        for projectile in self.projectiles:
            if projectile.speed > self.MAX_SPEED:
                raise ValueError("Object speed goes beyond env limit: "
                                 f"{projectile.speed} > {self.MAX_SPEED}.")
            dead_status.append(projectile.act(self.TIMESTEP))
        for ind in np.where(dead_status)[0][::-1]:
            self.projectiles.pop(ind)

        # WIP : add detection of simulation aberations i.e obj too fast or out
        # of map boundaries

    def _is_done(self):
        """Check if dodger agent is hit by a projectile."""
        # WIP : add timeout security ?
        for projectile in self.projectiles:
            dist = self.dodger_agent.get_distance(projectile.x, projectile.y)
            if dist < self.dodger_agent.RADIUS:
                # update agent status
                self.dodger_agent.is_hit = True
                return True
        return False

    def set_renderer(self,
                     mode=DEFAULT_RENDERER,
                     path=None,
                     generate_movie=False):
        """Initialise the viewer and its rendered mode.
        Can be used to turn on recording. To be called after reset"""
        self.render_mode = mode
        self.render_is_movie = generate_movie
        # Default path for movie generation if not passed
        # in args or not previously set.
        if path is None and self.render_path is None:
            self.render_path = self.DEFAULT_IMG_PATH + time.strftime(
                "%Y_%d_%m_%H_%M_%S")
        else:
            self.render_path = path
        # Create directory if it doesn't exist.
        if not os.path.isdir(self.render_path):
            os.makedirs(self.render_path)

        if self.viewer is not None:
            self.viewer.mode = self.render_mode
            self.viewer.path = self.render_path
            self.viewer.generate_movie = self.render_is_movie

    def render(self, mode=DEFAULT_RENDERER):
        '''
        Render the environment using the mode provided.
        Note that it can return visual data for later processing depending
        on the provided mode.
        '''
        if self.viewer is None:
            # automatic setup of renderer
            self.set_renderer(mode=mode)
            self.viewer = EnvViewer(env=self,
                                    mode=self.render_mode,
                                    path=self.render_path,
                                    generate_movie=self.render_is_movie)
        return self.viewer.display()

    def close(self):
        """Close the environement and viewer."""
        print("Closing the gym.")
        self.done = True
        if self.viewer is not None:
            self.viewer.close()
        self.viewer = None

    def set_reward_mode(self, mode):
        """Force set the reward mode."""
        self.reward_mode = RewardMode(mode)

    def set_shooter_number(self, number):
        """Force set the number of shooter agents."""
        if isinstance(number, int):
            self.n_shooter = number
        else:
            raise ValueError("Number of shooter agents set must be an integer"
                             f"{number} : {type(number)}.")

    def set_shooter_mode(self, mode):
        """Force set the mode of shooter agents."""
        self.shooter_mode = ShooterMode(mode).mode

    def set_action_space_type(self, action_space_type):
        """Force set the action space type of the environment."""
        self.action_space_type = action_space_type

    def set_observation_type(self, observation_type):
        """Force set the observation type of the environement."""
        self.observation_type = observation_type
