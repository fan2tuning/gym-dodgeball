import gym
import pytest

import gym_dodgeball  # noqa F401
from gym_dodgeball.envs.common.rewards import RewardMode
from gym_dodgeball.agents.shooter import ShooterMode
from gym.spaces import Discrete, MultiBinary
from gym_dodgeball.envs.common.observations import OmniscientObservation

ENV_ID = "dodgeball-v0"
REWARD_MODES = [mode.mode for mode in RewardMode]
SHOOTER_MODES = [mode.mode for mode in ShooterMode]
ACTION_SPACES = [Discrete, MultiBinary]
OBS_TYPES = [OmniscientObservation]


@pytest.mark.parametrize("reward_mode", REWARD_MODES)
def test_reward_modes(reward_mode):
    """Test the gym for all reward modes configurable."""
    env = gym.make(ENV_ID)
    env.set_reward_mode(reward_mode)

    env.reset()
    for i in range(3):
        action = env.action_space.sample()
        obs, reward, done, info = env.step(action)
    env.close()

    assert env.observation_space.contains(obs)
    assert 0 <= reward <= 1


@pytest.mark.parametrize(
    "number,expected",
    [(-1, 0), (0, 0), (4, 4), (6, 6),
     pytest.param("1", None, marks=pytest.mark.xfail(raises=ValueError)),
     pytest.param(5.4, None, marks=pytest.mark.xfail(raises=ValueError))])
def test_shooter_number(number, expected):
    """Test gym for a various amount of shooter agents."""
    env = gym.make(ENV_ID)
    env.set_shooter_number(number)
    env.reset()
    env.close()

    assert len(env.shooter_agents) == expected


@pytest.mark.parametrize("shooter_mode", SHOOTER_MODES)
def test_shooter_modes(shooter_mode):
    """Test the gym for all shooter mode configurable."""
    env = gym.make(ENV_ID)
    env.set_shooter_mode(shooter_mode)
    env.reset()
    env.close()

    for agent in env.shooter_agents:
        assert agent.mode.mode == shooter_mode


@pytest.mark.parametrize("action_space", ACTION_SPACES)
def test_action_spaces(action_space):
    """Test the gym for all possible action_space type."""
    env = gym.make(ENV_ID)
    env.set_action_space_type(action_space)
    env.reset()
    for i in range(3):
        action = env.action_space.sample()
        obs, reward, done, info = env.step(action)
    env.close()


@pytest.mark.parametrize("observation_type", OBS_TYPES)
def test_obs_types(observation_type):
    """Test the gym for all possible observation type"""
    env = gym.make(ENV_ID)
    env.set_observation_type(observation_type)
    env.reset()
    for i in range(3):
        action = env.action_space.sample()
        obs, reward, done, info = env.step(action)
        assert (env.observation_space.contains(obs))
    env.close()
