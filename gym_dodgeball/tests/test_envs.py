import gym
import pytest

import gym_dodgeball  # noqa F401

# loop over envs for a global test
envs = [
    "dodgeball-v0",
]


@pytest.mark.parametrize("env_spec", envs)
def test_env_step(env_spec):
    env = gym.make(env_spec)

    env.reset()
    for i in range(3):
        action = env.action_space.sample()
        obs, reward, done, info = env.step(action)
    env.close()

    assert env.observation_space.contains(obs)
