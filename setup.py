from setuptools import setup

setup(name='gym_dodgeball',
      version='0.0.1',
      install_requires=['gym==0.17.2',
                        'matplotlib==3.2.2',
                        'ffmpeg-python']
      )
